﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolManager : MonoBehaviour
{
    public List<Item> generateThesePools = new List<Item>();

    private List<ItemPool> itemPools = new List<ItemPool>();

    private void Awake()
    {
        foreach(Item item in generateThesePools) {
            GameObject g = new GameObject(item.name + "Pool");
            g.AddComponent<ItemPool>();
            ItemPool ip = g.GetComponent<ItemPool>();
            ip.Send(item, 10);
            itemPools.Add(ip);
        }
    }

    internal Item Get()
    {
        int i = UnityEngine.Random.Range(0, itemPools.Count);
        return itemPools[i].Get();
    }

    internal void ReturnObject(Item item)
    {
        item.transform.parent.GetComponent<ItemPool>().ReturnObject(item);
    }
}
