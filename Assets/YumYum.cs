﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class YumYum : MonoBehaviour {
    private float health = 100f;
    private float maxHealth = 100f;
    public float HealthPercent { get { return health / maxHealth; } }
    private float hunger = 100f;
    private float maxHunger = 100f;
    public float HungerPercent { get { return hunger / maxHunger; } }


    public Image healthBar;
    public Image hungerBar;

    private float hungerCounter = 0f;
    public float hungerTickSeconds = 1f;

    [SerializeField]
    [Range(0f, 10f)]
    private float primaryHeal;
    [SerializeField]
    [Range(0f, 10f)]
    private float primaryHunger;
    [SerializeField]
    [Range(0f, 10f)]
    private float primaryHurt;
    [SerializeField]
    [Range(0f, 10f)]
    private float primaryQuench;
    [SerializeField]
    [Range(0f, 10f)]
    private float secondaryHeal;
    [SerializeField]
    [Range(0f, 10f)]
    private float secondaryHunger;
    [SerializeField]
    [Range(0f, 10f)]
    private float secondaryHurt;
    [SerializeField]
    [Range(0f, 10f)]
    private float secondaryQuench;

    public Animator anim;
    public bool sickened;
    public bool dead = false;

    public float playersMaxScale = 2.5f;
    private bool angry = false;
    private bool eatingPlayer = false;

    public float ScalePrecentage { get { return transform.localScale.x / playersMaxScale; } }



    // Update is called once per frame
    void Update() {
        if (eatingPlayer)
            return;

        Debug.Log(ScalePrecentage);
        if (ScalePrecentage >= .9f) {
            EatPlayer();
            return;
        }

        if (dead) {
            GameManager.Instance.ShowRetry();
            return;
        }


        SwitchIdle();

        if (sickened) {
            health -= .1f;
            anim.SetBool("sick", true);
        } else {
            anim.SetBool("sick", false);
        }

        if (angry) {
            anim.SetBool("angry", true);
        } else {
            anim.SetBool("angry", false);
        }

        hungerCounter += Time.deltaTime;

        if (hungerCounter >= hungerTickSeconds) {
            hungerCounter = 0;
            hunger -= HungerAmmount();
        }

        if (health >= 100)
            health = 100;

        if (hunger >= 100)
            hunger = 100;

        if (health <= 0) {
            dead = true;
            anim.SetBool("dead", true);
            health = 0;
        }

        if (hunger <= 0)
            hunger = 0;

        if (hunger <= 20)
            health -= .05f;

        healthBar.fillAmount = Mathf.Clamp(HealthPercent, 0f, 1f);
        hungerBar.fillAmount = Mathf.Clamp(HungerPercent, 0f, 1f);
    }

    private void EatPlayer() {
        Debug.Log("eatPlayer");
        anim.SetBool("eatPlayer", true); // TODO Animat to player.
        eatingPlayer = true;
    }

    public void PlayerHasBeenEaten() {
        GameManager.Instance.LoadOutro();
    }

    IEnumerator UnSicken() {
        yield return new WaitForSeconds(UnityEngine.Random.Range(4, 8));
        sickened = false;
    }

    IEnumerator UnAngry() {
        yield return new WaitForSeconds(UnityEngine.Random.Range(5, 10));
        angry = false;
    }

    private void SwitchIdle() {
        bool change = false;
        bool faceRight = true;
        if (UnityEngine.Random.Range(0f, 1000f) >= 999f) {
            change = true;
        }
        if (change) {
            if (UnityEngine.Random.Range(0f, 10f) <= 5) {
                faceRight = false;
            }
            anim.SetBool("facingRight", faceRight);
        }



    }

    private float HungerAmmount() {
        // Change this to do more hunger damage based on difficulty
        return 20f * GameManager.Instance.DifficultyPercent;
    }

    private void OnTriggerEnter(Collider other) {
        if (dead)
            return;

        if (other.transform.tag == "Item") {
            if (!sickened) {
                EatItem(other.transform.GetComponent<ItemEffector>());
                other.transform.GetComponent<Item>().Return();
            }
        }
    }

    public void Eaten() {
        anim.SetBool("nom", false);
    }

    private void EatItem(ItemEffector itemEffector) {
        Debug.Log("scale");



        GameManager.Instance.PlayEat();
        anim.SetBool("nom", true);
        switch (itemEffector.primaryEffect) {
            case ItemEffector.Effector.HEAL:
                health += primaryHeal;
                GameManager.Instance.PlayYum();
                break;
            case ItemEffector.Effector.HUNGER:
                hunger -= primaryHunger;
                sickened = true;
                GameManager.Instance.PlaySick();
                StartCoroutine(UnSicken());
                break;
            case ItemEffector.Effector.HURT:
                health -= primaryHurt;
                angry = true;
                GameManager.Instance.PlayAngry();
                StartCoroutine(UnAngry());
                break;
            case ItemEffector.Effector.QUENCH:
                hunger += primaryQuench;
                GameManager.Instance.PlayYum();
                break;
            default:
                break;

        }

        switch (itemEffector.secondaryEffect) {
            case ItemEffector.Effector.HEAL:
                health += secondaryHeal;
                break;
            case ItemEffector.Effector.HUNGER:
                hunger -= secondaryHunger;
                break;
            case ItemEffector.Effector.HURT:
                health -= secondaryHurt;
                break;
            case ItemEffector.Effector.QUENCH:
                hunger += secondaryQuench;
                break;
            default:
                break;
        }
        Vector3 lscale = transform.localScale;
        lscale = lscale * 1.05f;

        //lscale.z = transform.localScale.z;
        transform.localScale = lscale;
    }
}
