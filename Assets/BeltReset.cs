﻿using UnityEngine;

public class BeltReset : MonoBehaviour
{
    public Transform beltSpawn;

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "BeltRemove")
        {
            transform.position = beltSpawn.position;
        }
    }
}
