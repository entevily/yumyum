﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroController : MonoBehaviour
{
    public AudioSource clip;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (clip.isPlaying)
            return;

        if (SceneManager.GetActiveScene().buildIndex == 4)
            
        {
            SceneManager.LoadScene("main_menu");
            return;
        }
        else if(SceneManager.GetActiveScene().buildIndex == 3)
        {
            SceneManager.LoadScene("credits");
            return;
        }

        SceneManager.LoadScene("main");
    }
}
