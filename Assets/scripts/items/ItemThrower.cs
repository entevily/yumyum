﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemThrower : MonoBehaviour
{
    public float dragSpeedDamper = 5.0f;

    private bool draggingMouse = false;
    private Plane dragPlane;
    private Vector3 moveToward;

    private void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        float distance;

        if (Input.GetMouseButtonDown(0))
        {
            
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform == transform)
                {
                    draggingMouse = true;
                    GetComponent<Rigidbody>().useGravity = false;
                    dragPlane = new Plane(-ray.direction.normalized, hit.point);
                }
            }
        }

        if (draggingMouse)
        {
            var hasHit = dragPlane.Raycast(ray, out distance);

            if (hasHit)
            {
                moveToward = ray.GetPoint(distance);
            }
        }

        if (Input.GetMouseButtonUp(0) && draggingMouse)
        {
            draggingMouse = false;
            GetComponent<Rigidbody>().useGravity = true;
            GetComponent<ItemMover>().enabled = false;
        }
    }

    public void Reset()
    {
         draggingMouse = false;
        Plane dragPlane = new Plane();
        Vector3 moveToward = Vector3.zero;
}

    private void FixedUpdate()
    {
        if (!draggingMouse) return;

        var velocity = moveToward -transform.position;
        velocity *= 10;
        velocity.z = -velocity.z;
        GetComponent<Rigidbody>().velocity = Vector3.Lerp(GetComponent<Rigidbody>().velocity, velocity, dragSpeedDamper * Time.deltaTime);

        
    }
}
